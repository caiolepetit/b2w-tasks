package solution;

import java.util.Scanner;

public class Solution {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int cont = 0;
		int numberOfZero = 0;
		
		Scanner s = new Scanner(System.in);

	    System.out.print("Digite um número inteiro: ");
		int input = s.nextInt();
		
		while(input !=0) {
			  if (input % 10 == 0) {
				  numberOfZero++;
			  }
			  input = input/10;
		      cont++;
		}
		
		if (numberOfZero != 0) {
			System.out.println("Quantidade de número reorganizados: " + (fatorial(cont) - (numberOfZero * 2)));
		} else {
			System.out.println("Quantidade de número reorganizados: " + (fatorial(cont)));
		}
	}
	
	public static int fatorial(int number){
		int i;
		int fat = 1;
		for (i = 1;i <= number;i++){
			fat=fat*i;
		}
		return fat;
	}

}
